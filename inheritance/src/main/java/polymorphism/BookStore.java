package polymorphism;

public class BookStore {

    public static void main(String[] args) {

        Book[] books = new Book[5];

        books[0] = new Book("A", "AuthorA");
        books[2] = new Book("C", "AuthorC");

        books[1] = new ElectronicBook("B", "AuthorB", 2);
        books[3] = new ElectronicBook("D", "AuthorD", 4);
        books[4] = new ElectronicBook("E", "AuthorE", 5);

        for (Book x : books) {
            System.out.println(x);
        }
    }
}