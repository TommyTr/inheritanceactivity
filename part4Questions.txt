•	When you printed the 1st Book of the array, which toString() method was called?
    The method from the Book class

•	When you printed the 2nd Book of the array, which toString() method was called
    The method from the ElectronicBook class

•	The first Book in the array actually is pointing at a Book. Try calling the method getNumberOfBytes() on this object. What happens? If there is an error, then what sort of error is there?
books[0].getNumberOfBytes()

    The method is undefined in the Book class.

•	The second Book in the array actually is pointing at an ElectronicBook. Try calling the method getNumberOfBytes() on this object. What happens? If there is an error, then what sort of error is there? 
books[1].getNumberOfBytes()

    The reference type determines what method can be called on. In that case, that method is still undefined in the Book class

•	The second Book in the array is actually pointing at an ElectronicBook. Try assigning the value to a variable of type ElectronicBook. What happens? If there is an error, then what sort of error is there?

ElectronicBook b = books[1];

    Cannot convert from Book to ElectronicBook error

•	What happens if you add a cast to cast books[1] to an ElectronicBook? If there is an error, then what sort of error is there?

ElectronicBook b = (ElectronicBook)books[1];

    It works. Because casting converts the object from one type to another.
    
•	Follow up to the previous question: What happens if you try to call the getNumberOfBytes() method on the object b? If there is an error, then what sort of error is there?

System.out.println(b.getNumberOfBytes());
    
    The reference type determines the method that's being called. ElectronicBook has the getNumberOfBytes method.
    
•	Repeat the last 2 steps again, but do them on books[0], which is actually pointing at a Book. What happens? If there is an error, then what sort of error is there?


